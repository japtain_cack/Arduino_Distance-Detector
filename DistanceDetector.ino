#include "pitches.h"
// ==========================
// Global Variables
// ==========================

// Constants
const int NbrLEDs = 3;                                // set the number of LEDs
const int ledPins[] = {3, 4, 5};                      // Set the LED pins
const int relayOne = 0;                               // Set the first relay pin
const int relayTwo = 1;                               // Set the second relay pin
const int trigPin = 6;                                // Set the trigger pin (send)
const int echoPin = 7;                                // Set the echo pin (receive)
const int buzzer = 2;                                 // Set the buzzer pin
const int loopDelay = 100;                            // Delay at which the program loops (affects sensitivity)

// Variables
int ledLevel = 0;                                     // Initialize the LED light bar
int inches = 0;                                       // Initialize inches variable
int lastDistance = 0;                                 // Value for last read distance
int alerted = 0;                                      // Prevent continuous alerting

// You can change these
bool serialEnabled = false;                           // Enable serial output (debug mode)
bool silentMode = false;                              // Disable buzzer
int maxDistance = 100;                                // Set the maximum usable distance; this is based on your sensor
int threashold = 10;                                  // Threshold for change in distance required to trip the alert
int alertDelay = 10000;                               // After alarm sounds, wait x miliseconds before next check
int reinit = 2;                                       // Nummber of alertDelay seconds before sensor is reinitialized with new values
int sound = 1000;                                     // Set the pitch of the buzzer


// ==========================
// Initialization
// ==========================
void setup() {
  if (serialEnabled) {
    Serial.begin(9600);
  }

  // make all the LED pins outputs
  for (int led = 0; led < NbrLEDs; led++) {
    pinMode(ledPins[led], OUTPUT);
  }
  // Set pin in/outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(relayOne, OUTPUT);
  pinMode(relayTwo, OUTPUT);

  // Get current distance
  lastDistance = distanceInches();

  // Setup finished play LED animation
  ledTrail(2);
  if (!silentMode){
    marioOneUp();
  }
}


// ==========================
// Distance detection
// ==========================
void loop() {
  alerted = 0;
  inches = distanceInches();

  // Translate the value from the sensor to x number of LEDs
  ledLevel = map(inches, 0, maxDistance, 0, NbrLEDs);
  ledsOff();
  lightLEDs(ledLevel);

  // Alert while a change in proximity has exceeded the threshold
  while (lastDistance - inches >= threashold && alerted < reinit) {
    // Enable relay 1 and disable relay 2
    digitalWrite(relayTwo, LOW); digitalWrite(relayOne, HIGH);
    if (!silentMode){
      marioCoin();
    }
    ledFlash(2);
    delay(alertDelay);
    inches = distanceInches();
    alerted++;
  }
  lastDistance = inches;
  digitalWrite(relayOne, LOW); digitalWrite(relayTwo, HIGH);

  // Output serial data if enabled
  if (serialEnabled) {
    Serial.print(inches);
    Serial.print("in");
    Serial.println();
  }

  delay(loopDelay);
}


// ==========================
// Functions
// ==========================

// Turn off all LEDs
void ledsOff() {
  for (int led = 0; led < NbrLEDs; led++) {
    digitalWrite(ledPins[led], LOW);
  }
}

// Enable x number of LEDs
void lightLEDs(int level) {
  for (int led = 0; led < NbrLEDs; led++) {
    if (led < level) {
      digitalWrite(ledPins[led], HIGH);
    }
  }
}

// Get distance in inches
int distanceInches() {
  int inch = 0;
  long duration = 0;

  // The PING is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Read the value from the ultrasonic sensor
  duration = pulseIn(echoPin, HIGH);

  // Convert microseconds to inches
  inch = microsecondsToInches(duration);

  return inch;
}

long microsecondsToInches(long microseconds) {
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds) {
  // The speed of sound is 340 m/s or 29 microseconds per centimetre.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}


// ==========================
// Sounds
// ==========================
void marioCoin() {
  // Play coin sound
  tone(buzzer, NOTE_B5, 100);
  delay(100);
  tone(buzzer, NOTE_E6, 850);
  delay(800);
  noTone(8);
}

void marioOneUp() {
  // Play 1-up sound
  tone(buzzer, NOTE_E6, 125);
  delay(130);
  tone(buzzer, NOTE_G6, 125);
  delay(130);
  tone(buzzer, NOTE_E7, 125);
  delay(130);
  tone(buzzer, NOTE_C7, 125);
  delay(130);
  tone(buzzer, NOTE_D7, 125);
  delay(130);
  tone(buzzer, NOTE_G7, 125);
  delay(125);
  noTone(8);
}

void marioFireball() {
  // Play Fireball sound
  tone(buzzer, NOTE_G4, 35);
  delay(35);
  tone(buzzer, NOTE_G5, 35);
  delay(35);
  tone(buzzer, NOTE_G6, 35);
  delay(35);
  noTone(8);
}


// ==========================
// LED Animations
// ==========================

// LED chaser animation
void ledTrail(int iterations) {
  for (int iteration = 0; iteration < iterations; iteration++) {
    for (int led = 0; led < NbrLEDs; led++) {
      digitalWrite(ledPins[led], HIGH);
      delay(50);
    }
    for (int led = 0; led < NbrLEDs; led++) {
      digitalWrite(ledPins[led], LOW);
      delay(50);
    }
  }
}

// LED bounce animation
void ledBounce(int iterations) {
  for (int iteration = 0; iteration < iterations; iteration++) {
    for (int led = 0; led < NbrLEDs; led++) {
      digitalWrite(ledPins[led], HIGH);
      delay(50);
    }
    for (int led = NbrLEDs; led > 0; led--) {
      digitalWrite(ledPins[led], LOW);
      delay(50);
    }
  }
}

// LED flash animation
void ledFlash(int iterations) {
  for (int iteration = 0; iteration < iterations; iteration++) {
    for (int led = 0; led < NbrLEDs; led++) {
      digitalWrite(ledPins[led], HIGH);
    }
    delay(50);
    for (int led = 0; led < NbrLEDs; led++) {
      digitalWrite(ledPins[led], LOW);
    }
    delay(50);
  }
}
